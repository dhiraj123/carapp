import React, {Component} from "react";
class LeftPanel extends Component {
   
    handleChange=(e)=>{
        let s1={...this.props.options};
        let {currentTarget:input}=e
        s1[input.name]=input.value
        this.props.onOptionChange(s1);
        
    }
    render(){
      const {allOptions,options}=this.props
      console.log(options)
      let {fuel,type,sort}=options
        return (
            <div className="container">
               
            
            {this.showRadios("Fuel",allOptions.fuel,"fuel",fuel)}
            <br/>
            {this.showRadios("Type",allOptions.type,"type",type)}
            <br/>
            {this.showRadios("Sort",allOptions.sort,"sort",sort)}
                </div>
        )
    }
    
    showRadios=(label,arr,name,selVal)=>{
        return (
            <div className="col-12 ">
                <label className=" col-12 form-check-label font-weight-bold border">{label}</label><br/>
                    {arr.map((p1)=>(
                        <div className="form-check border">
                        <input type="radio" className="form-check-input" 
                         name={name} 
                         value={p1}
                        checked={selVal===p1}
                         onChange={this.handleChange}/>
                        <label className="form-check-label">{p1}</label>
                        </div>
                    ))}
                </div>
        )
    }
}
export default LeftPanel;