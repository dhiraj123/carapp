import React, {Component} from "react";
import http from "./httpService.js"
class SimpleForm extends Component {
    state ={
        cars:{id:"",price:"",year:"",kms:"",model:"",color:""},
        colors: ["White", "Silver Grey", "Metallic Blue", "Red"],
        Model:["Swift Dzire VXi","Etios SMi","City AXi","Swift DXi","Etios VXi","City ZXi"],
        edit:false,

    };
    async fetchData(){
        const {id}=this.props.match.params;
        console.log(id)
        if (id) {
            let response= await http.get(`/svr/cars/${id}`);
            let {data}=response;
           console.log(data);
            this.setState({cars:data,edit:true})
        }
        else {
            let cars={id:"",price:"",year:"",kms:"",model:"",color:""};
            this.setState({cars:cars,edit:false});
        }
    }
    async componentDidMount(){
        this.fetchData();
    }
    async componentDidUpdate(prevProps,prevState){
        if (prevProps!==this.props)
             this.fetchData();
    }
    
    handleChange=(e)=>{
       const {currentTarget: input}=e
        let s1={...this.state}
        s1.cars[input.name]=input.value
        this.setState(s1);
    }
    async postData(url,obj){
        let response= await http.post(url,obj);
        let {data}=response;
        console.log(data);
        this.props.history.push("/cars")

    }

    async putData(url,obj){
        let response= await http.put(url,obj);
        let {data}=response;
        console.log(data);
        this.props.history.push("/cars")

    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let {cars,edit}=this.state;
        edit ? this.putData(`/svr/cars/${cars.id}`,cars)
        :this.postData("/svr/cars",cars)  
    };

    render(){
        let {id,price,year,kms,model,color}=this.state.cars;
        const {Model,colors,edit}=this.state;
        return (
            <div className="container">
                    <div className="form-group">
                    <label>Car ID</label>
                    <input type="text" disabled={edit===true} className="form-control" id="id" name="id" value={id} 
                    placeholder="Enter Id" onChange={this.handleChange}/>
                    </div>

                    <div className="form-group">
                    <label>Price</label>
                    <input type="text" className="form-control" id="price" name="price" value={price} 
                    placeholder="Enter Price" onChange={this.handleChange}/>
                    </div>

                    <div className="form-group">
                    <label>Mileage in kms</label>
                    <input type="text" className="form-control" id="kms" name="kms" value={kms} 
                    placeholder="Enter mileage" onChange={this.handleChange}/>
                    </div>

                    <div className="form-group">
                    <label>Year of Manufacture</label>
                    <input type="text" className="form-control" id="year" name="year" value={year} 
                    placeholder="Enter year" onChange={this.handleChange}/>
                    </div>
                    <div className="col-12">
                <div className="row">
<div className="col-6">Model</div>
<div className="col-6">Color</div>
                    </div>
                   </div>
                   <div className="col-12">
                       <div className="row">
                           <div className="col-6">
                   <div className="form-group">
                       <select className="form-control" name="model" value={model} onChange={this.handleChange}>
                           <option value="">Select Model</option>
                           {Model.map((pr)=>(
                               <option value={pr}>{pr}</option>
                           ))}
                           </select>
                         </div>
                         </div>
                         <div className="col-6">
                         <div className="form-group">
                       <select className="form-control" name="color" value={color} onChange={this.handleChange}>
                           <option value="">Select Color</option>
                           {colors.map((pr)=>(
                               <option value={pr}>{pr}</option>
                           ))}
                           </select>
                           </div>
                         </div>
                         </div>
                         </div>
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </div>
        )

    }
}
export default SimpleForm;