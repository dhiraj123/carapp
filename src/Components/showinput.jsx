import React, {Component} from "react";
class SimpleForm extends Component {
    
    handleChange=(e)=>{
        let s1={...this.props.options};
        let {currentTarget:input}=e
        s1[input.name]=input.value
        this.props.onOptionChange(s1);
        

    }
    render(){
        let {minprice,maxprice}=this.props.options
        return (
           
               <div className="col-12">
                   <h4 className="text-center">All Cars</h4>
                   <div className="row">
                       PriceRange:
                   <div className="col-4">
                <div className="form-group">
                    <input type="text" className="form-control" id="minprice" name="minprice" value={minprice} 
                    placeholder="Enter minPrice" onChange={this.handleChange}/>
                    </div>
</div>
<div className="col-4">
                    <div className="form-group">
                    <input type="text" className="form-control" id="maxprice" name="maxprice" value={maxprice} 
                    placeholder="Enter Maxprice" onChange={this.handleChange}/>
                    </div>
                    </div>
                </div>
                </div>
        )

    }
}
export default SimpleForm;