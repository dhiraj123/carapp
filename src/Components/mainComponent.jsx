import React, {Component} from "react";
import { Route,Switch,Redirect} from "react-router-dom";
import NavBar from "./navbar";
import Cars from "./cars"
import Car from "./car"
import DeleteCar from "./deletecar"
class MainComponent extends Component {
    
    render(){
        return (
            <div className="container">
                <NavBar  />
                <Switch>
                <Route path="/car/:id" component={Car} />
                <Route path="/cars/:id/delete" component={DeleteCar} />
                <Route path="/cars" component={Cars} />
                <Route path="/car" component={Car} />
                
                    </Switch>
                </div>
        )
    }
}
export default MainComponent; 