import React, {Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "./httpService";
import ShowOption from "./leftpanel";
import SimpleForm from "./showinput";
class Cars extends Component{
    state={
        lists:[],
        allOptions:{
            fuel:["Diesel","Petrol"],
            type:["Hatchback","Sedan"],
            sort:["kms","price","year"]
        }
       
    }
    async fetchData(){
        let queryParams=queryString.parse(this.props.location.search);
        console.log(queryParams)
        let searchStr=this.makeSearchString(queryParams);
        console.log(searchStr)
        let response= await http.get(`/svr/cars?${searchStr}`)
        console.log(response)
        let {data}=response
        this.setState({lists:data})
    }
    componentDidMount(){
        this.fetchData();
    }
    
    componentDidUpdate(prevProps,prevState){
        if (prevProps !== this.props) this.fetchData();
    }

    handleOptionChange=(options)=>{
        console.log(options)
        this.callURL("/cars",options)
    };

    callURL=(url,options)=>{
        let searchString=this.makeSearchString(options);
        this.props.history.push({
            pathname:url,
            search:searchString,
        });
    };
    makeSearchString=(options)=>{
        let {minprice,maxprice,fuel,sort,type}=options;
        let searchStr=""
        searchStr=this.addToQueryString(searchStr,"minprice",minprice)
        searchStr=this.addToQueryString(searchStr,"maxprice",maxprice)
        searchStr=this.addToQueryString(searchStr,"fuel",fuel)
        searchStr=this.addToQueryString(searchStr,"sort",sort)
        searchStr=this.addToQueryString(searchStr,"type",type)
        return searchStr; 
    }

    addToQueryString = (str,paramName,paramValue)=>
        paramValue
         ? str
          ? `${str}&${paramName}=${paramValue}`
          : `${paramName}=${paramValue}`
        : str;


   
    render(){
    const {lists,allOptions}=this.state
    let queryParams=queryString.parse(this.props.location.search);
    console.log(queryParams)
        return(
            <div className="container">
                <div className="row">
                    <div className="col-3">
                   <ShowOption allOptions={allOptions} options={queryParams} onOptionChange={this.handleOptionChange}/>
                        </div>
                    <div className="col-9">
                        <SimpleForm  options={queryParams} onOptionChange={this.handleOptionChange}/>
                    <div className="row ">
                {lists.map((pr)=>(
                    
                      <div className="col-3 bg-warning border text-center">
                          {pr.model}
                          <br/>
                          Price : <i className="fas fa-rupee-sign"></i>{pr.price}
                          <br/>
                        Color : {pr.color}
                        <br/>
                        Mileage : {pr.kms}
                        <br/>
                        Manufacture in {pr.year}
                        <br/>
                <Link className="text-dark" to={`/car/${pr.id}`}><i className="fas fa-edit" style={{float: "left"}} ></i></Link>
                    <Link className="text-dark" to={`/cars/${pr.id}/delete`}><i className="fas fa-trash-alt" style={{float: "right"}}></i></Link>
                          </div>
                        
                ))}
                </div>
                 </div> 
            </div>
            </div>
        ) 
    }
}
export default Cars;